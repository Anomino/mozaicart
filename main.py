import cv2
import numpy as np
import os

INPUT_IMG = 'input/fure.jpeg'
TEMPLETE_PATH = 'templete/'

class ImageDividionException(Exception):
    pass


def getSimByCCORR(org_img, com_img):
    """
    入力画像と比較画像から、類似度の算出
    手法は、相関係数マッチング
    :param org_img:
    :param com_img:
    :return:
    """

    # cv2.imshow('org', org_img)
    # cv2.imshow('com', com_img)
    # cv2.waitKey(0)

    # 相関係数マッチング
    matches = cv2.matchTemplate(org_img, com_img,cv2.TM_CCORR_NORMED)

    # カラーヒストグラムによる類似度算出
    # color = ('b', 'g', 'r')
    # org_modes = []
    # for num, col in enumerate(color):
    #     tmp_hist = cv2.calcHist([org_img], [num], None, [32], [0, 255])
    #     org_modes.append(np.argmax(tmp_hist))
    #
    # com_modes = []
    # for num, col in enumerate(color):
    #     tmp_hist = cv2.calcHist([com_img], [num], None, [32], [0, 255])
    #     com_modes.append(np.argmax(tmp_hist))
    #
    # matches = 0
    # for org_v, com_v in zip(org_modes, com_modes):
    #     matches += np.linalg.norm(org_v - com_v)
    #
    # # 正規化
    # matches = 1 - (matches / 765)
    return matches.mean()


def getSimByHist(org_img, com_img):
    """
    入力画像と比較画像から、類似度の算出
    手法は、相関係数マッチング
    :param org_img:
    :param com_img:
    :return:
    """
    # カラーヒストグラムによる類似度算出
    color = ('b', 'g', 'r')
    org_modes = []
    for num, col in enumerate(color):
        tmp_hist = cv2.calcHist([org_img], [num], None, [32], [0, 255])
        org_modes.append(np.argmax(tmp_hist))

    com_modes = []
    for num, col in enumerate(color):
        tmp_hist = cv2.calcHist([com_img], [num], None, [32], [0, 255])
        com_modes.append(np.argmax(tmp_hist))

    matches = 0
    for org_v, com_v in zip(org_modes, com_modes):
        matches += np.linalg.norm(org_v - com_v)

    # 正規化
    matches = 1 - (matches / 765)
    return matches


def divideImg(img, x_divided, y_divided):
    """
    画像を指定した分割数に分割する。
    分割した画像リストを返却する。
    :param img:
    :param x_divided:
    :param y_divided:
    :return:
    """
    img_y = img.shape[0]
    img_x = img.shape[1]

    if not (img_x % x_divided) == 0 and (img_y % y_divided) == 0:
        raise ImageDividionException('divided constant is not valid')

    x_step, y_step = img_x / x_divided, img_y / y_divided

    divided_img = []
    tmp_divided = np.hsplit(img, x_step)
    for row_divided in tmp_divided:
        col_divided = np.vsplit(row_divided, y_step)
        divided_img.append(col_divided)

    return divided_img, int(img_x / x_step), int(img_y / y_step)


def createMozaic(org_path, com_path, x_divided, y_divided):
    """

    :param org_path:
    :param com_path:
    :param x_divided:
    :param y_divided:
    :return:
    """
    org_img = cv2.imread(org_path)
    mozaic_img = org_img.copy()
    com_imgs = []

    try:
        divided_img, block_x, block_y = divideImg(org_img, x_divided, y_divided)
        mozaic_index = []

        com_list = os.listdir(com_path)
        for files in com_list:
            com_img = cv2.imread(com_path + files)
            res_com = cv2.resize(com_img, (block_x, block_y))
            com_imgs.append(res_com)

        for num, x_div in enumerate(divided_img):
            print('{0} row searching'.format(num))
            tmp_index = []
            for y_div in x_div:
                # cv2.imshow('test', y_div)
                # cv2.waitKey(0)
                # ブロックごとに類似度が最大のテンプレート画像を探す
                max_com = [0, 0]
                for num, com in enumerate(com_imgs):
                    sim = getSimByHist(y_div, com)

                    if max_com[0] < sim:
                        max_com[0] = sim
                        max_com[1] = num

                tmp_index.append(max_com[1])

            mozaic_index.append(tmp_index)

        for x, x_mozaic in enumerate(mozaic_index):
            for y, tmp_index in enumerate(x_mozaic):
                mozaic_img[y * block_y : (y + 1) * block_y, x * block_x : (x + 1) * block_x] = com_imgs[tmp_index]

        cv2.imshow('org', org_img)
        cv2.imshow('mozaic', mozaic_img)
        cv2.waitKey(0)

    except ImageDividionException:
        print('dividion Error')
        exit()

if __name__ in '__main__':
    createMozaic(INPUT_IMG, TEMPLETE_PATH, 5, 4)
